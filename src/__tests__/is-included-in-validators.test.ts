import { Validator } from 'class-validator';
import { IsIncludedIn } from '../validators';

class MyClass {
    @IsIncludedIn(['word'])
    words: string;
}

describe('Exact Match Validator', () => {
    let validator: Validator;

    beforeEach(() => {
        validator = new Validator();
    });
    test('should fail exact match verifications', async () => {
        const model = new MyClass();
        model.words = 'not included';
        const errors = await validator.validate(model);
        expect(errors.length).toEqual(1);
    });
    test('should pass match verifications', async () => {
        const model = new MyClass();
        model.words = 'word';
        const errors = await validator.validate(model);
        expect(errors.length).toEqual(0);
    });
});
