import { ValidationOptions, registerDecorator, ValidationArguments } from 'class-validator';
import { parsePhoneNumberFromString, CountryCode } from 'libphonenumber-js';
import { isEmpty } from 'lodash';

interface IsGBPhoneParams {
    dev: CountryCode[];
    prod: CountryCode[];
}

export function IsGBPhoneNumber(params: IsGBPhoneParams, validationOptions?: ValidationOptions) {
    return (object: Record<string, any>, propertyName: string) => {
        registerDecorator({
            name: 'IsGBPhoneNumber',
            target: object.constructor,
            propertyName,
            constraints: [[...params.dev], [...params.prod]],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const [validDevRegions, validProdRegions] = args.constraints;

                    if (typeof value !== 'string') return false;

                    const parsed = parsePhoneNumberFromString(value);

                    if (!parsed) return false;

                    if (process.env.NODE_ENV === 'production') {
                        return !isEmpty(validProdRegions) ? validProdRegions.includes(parsed.country) : true;
                    }

                    return !isEmpty(validDevRegions) ? validDevRegions.includes(parsed.country) : true;
                },
            },
        });
    };
}
