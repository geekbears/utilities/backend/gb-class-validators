import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';

export function IsIncludedIn(property: string[], validationOptions?: ValidationOptions) {
    return (object: object, propertyName: string) => {
        registerDecorator({
            name: 'isIncludedIn',
            target: object.constructor,
            propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value: string, args: ValidationArguments) {
                    const [array] = args.constraints as string[][];
                    return array.includes(value);
                },
            },
        });
    };
}
