import { Validator } from 'class-validator';
import { IsGBPhoneNumberV2 } from '../validators';

class TestClass {
    @IsGBPhoneNumberV2()
    phone: any;
}

describe('Phone Validator V2', () => {
    let validator: Validator;

    beforeEach(() => {
        validator = new Validator();
    });

    test('should fail when not a string', async () => {
        const model = new TestClass();
        model.phone = 98234;
        const errors = await validator.validate(model);
        expect(errors.length).toBeGreaterThan(0);
    });

    test('should fail when providing a non-international phone number (missing country code)', async () => {
        const model = new TestClass();
        model.phone = '2025550115';
        const errors = await validator.validate(model);
        expect(errors.length).toBeGreaterThan(0);
    });

    test('should fail when providing an invalid phone number (incorrect format)', async () => {
        const model = new TestClass();
        model.phone = '98234';
        const errors = await validator.validate(model);
        expect(errors.length).toBeGreaterThan(0);
    });

    test('should fail when providing a phone number with an invalid country code', async () => {
        const model = new TestClass();
        model.phone = '+535522626391';
        const errors = await validator.validate(model);
        expect(errors.length).toBeGreaterThan(0);
    });

    test('should pass when providing a valid phone number with an international code', async () => {
        const model = new TestClass();
        model.phone = '+44 7911 123456';
        const errors = await validator.validate(model);
        expect(errors.length).toEqual(0);
    });

    test("should fail when providing a phone number that can't be parsed by google-libphonenumber", async () => {
        const model = new TestClass();
        model.phone = '+12345';
        const errors = await validator.validate(model);
        expect(errors.length).toBeGreaterThan(0);
    });

    test('should fail when providing an incomplete phone number', async () => {
        const model = new TestClass();
        model.phone = '+44 791';
        const errors = await validator.validate(model);
        expect(errors.length).toBeGreaterThan(0);
    });
});
