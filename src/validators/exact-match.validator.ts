import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';
import { isEqual } from 'lodash';

export function ExactMatch(property: string, validationOptions?: ValidationOptions) {
    return (object: object, propertyName: string) => {
        registerDecorator({
            name: 'exactMatch',
            target: object.constructor,
            propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value: any, args: ValidationArguments) {
                    const [relatedPropertyName] = args.constraints;
                    const relatedValue = (args.object as any)[relatedPropertyName];
                    return isEqual(value, relatedValue);
                },
            },
        });
    };
}
