import { Validator } from 'class-validator';
import { before } from 'lodash';
import { ExactMatch } from '../validators';

class MyClass {
    @ExactMatch('lastName', {
        message: '$property must be longer then $constraint1. Given value: $value',
    })
    firstName: string;
    lastName: string;
}

describe('Exact Match Validator', () => {
    let validator: Validator;

    beforeEach(() => {
        validator = new Validator();
    });
    test('should fail exact match verifications', async () => {
        const model = new MyClass();
        model.firstName = 'Name';
        const errors = await validator.validate(model);
        expect(errors.length).toEqual(1);
    });
    test('should pass match verifications', async () => {
        const model = new MyClass();
        model.firstName = 'Name';
        model.lastName = 'Name';
        const errors = await validator.validate(model);
        expect(errors.length).toEqual(0);
    });
});
