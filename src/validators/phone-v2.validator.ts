/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { ValidationOptions, registerDecorator, ValidationArguments } from 'class-validator';
import { PhoneNumberUtil } from 'google-libphonenumber';

export function IsGBPhoneNumberV2(validationOptions?: ValidationOptions) {
    return function (object: object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName,
            options: validationOptions,
            constraints: [],
            validator: {
                validate(value: any, args: ValidationArguments) {
                    let isValid = false;
                    if (typeof value !== 'string') return isValid;
                    const phoneUtil = PhoneNumberUtil.getInstance();
                    try {
                        const parsedNumber = phoneUtil.parseAndKeepRawInput(value, '');
                        isValid = phoneUtil.isValidNumber(parsedNumber);
                    } catch (error) {
                        return isValid;
                    }
                    return isValid;
                },

                defaultMessage(args: ValidationArguments) {
                    return 'The phone number ($value) is not valid!';
                },
            },
        });
    };
}
